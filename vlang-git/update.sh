#!/usr/bin/env bash

## Neko-san <nekoNexus at protonmail dot ch> got the relevant knowledge for this script from:
# https://gist.github.com/steinwaywhw/a4cd19cda655b8249d908261a62687f8#gistcomment-2736901
# https://codefather.tech/blog/bash-get-script-directory/

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)


echo 'Cleaning old source(s) in current directory...' & wait $last_pid
for file in "${SCRIPT_DIR}"/*.tar.gz; do rm "${file}"; done


auto_arch=$(cat /etc/pacman.conf | grep -w "^Architecture =" | sed 's/Architecture = //')
if [ ! "${auto_arch}" != "auto" ]; then
    echo "${auto_arch}" | xargs -I {} sed -Ei "s/arch=.*'([^']+)'.*/arch=({})/g" PKGBUILD
    if [[ $? != 0 ]]; then
        echo "An error occurred in update.sh"
        exit 1
    fi
else
    auto_arch=$(cat /etc/makepkg.conf | grep -w "^CARCH" | sed 's/CARCH=//' | sed 's/\"//g')
    echo "${auto_arch}" | xargs -I {} sed -Ei "s/arch=.*'([^']+)'.*/arch=({})/g" PKGBUILD
    if [[ $? != 0 ]]; then
        echo "An error occurred in update.sh"
        exit 1
    fi
fi

touch .SRCINFO
rm .SRCINFO && makepkg --printsrcinfo >> .SRCINFO

echo "PKGBUILD update complete."
