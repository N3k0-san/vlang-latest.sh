#!/usr/bin/env bash

## Neko-san <nekoNexus at protonmail dot ch> got the relevant knowledge for this script from:
# https://gist.github.com/steinwaywhw/a4cd19cda655b8249d908261a62687f8#gistcomment-2736901
# https://codefather.tech/blog/bash-get-script-directory/

SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)


echo 'Cleaning old source(s) in current directory...' & wait ${last_pid}
for file in "${SCRIPT_DIR}"/*.tar.gz; do rm "${file}"; done


auto_arch=$(cat /etc/pacman.conf | grep -w "^Architecture =" | sed 's/Architecture = //')
if [ ! "${auto_arch}" != "auto" ]; then
    echo "${auto_arch}" | xargs -I {} sed -Ei "s/arch=.*'([^']+)'.*/arch=({})/g" PKGBUILD
    if [[ $? != 0 ]]; then
        echo "An error occurred in update.sh"
        exit 1
    fi
else
    auto_arch=$(cat /etc/makepkg.conf | grep -w "^CARCH" | sed 's/CARCH=//' | sed 's/\"//g')
    echo "${auto_arch}" | xargs -I {} sed -Ei "s/arch=.*'([^']+)'.*/arch=({})/g" PKGBUILD
    if [[ $? != 0 ]]; then
        echo "An error occurred in update.sh"
        exit 1
    fi
fi


echo "Updating the PKGBUILD version..."
curl --silent "https://api.github.com/repos/vlang/v/releases/latest" |
                grep '"tag_name":' |                                                 
                sed -E 's/.*"([^"]+)".*/\1/' |
                xargs -I {} sed -Ei 's/pkgver=.*.*/pkgver={}/g' PKGBUILD & wait ${last_pid}

echo "Grabing the latest release source for SHA256sum verification..."
curl --silent "https://api.github.com/repos/vlang/v/releases/latest" |
                grep '"tag_name":' |                                                 
                sed -E 's/.*"([^"]+)".*/\1/' |
                xargs -I {} sh -c 'curl -sOL "https://github.com/vlang/v/archive/{}.tar.gz" & wait ${last_pid} && sha256sum "{}.tar.gz" | cut -d " " -f 1' |
                xargs -I {} sed -Ei "s/sha256sums=.*'([^']+)'.*/sha256sums=('{}')/g" PKGBUILD && wait ${last_pid}

touch .SRCINFO
rm .SRCINFO && makepkg --printsrcinfo >> .SRCINFO

echo "PKGBUILD update complete."
