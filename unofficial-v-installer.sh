#!/usr/bin/env bash

### Disclaimer: I wrote this script while learning bash at the same time; thus, it will likely appear unnecessarily lengthy, which is more than likely true
### This can definitely be done better but it works and should have only very minor issues

START_DIR=${PWD}

## Ignore these two variables or now; I was planning to do something with them but I forgot what it was, so I'm leaving them here until I remember
_SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
SCRIPT_DIR="${_SCRIPT_DIR}"

## This is for user-personal use
PERSONAL_WORK_DIR="/mnt/extraStorage/AUR/paru/clone"

if [ ! -f "$(which makepkg)" ]; then
    echo 'This script only works on pacman-based Linux distrobutions.'
    return | &wait "${last_pid}"
    if [[ $? != 0 ]]; then
        return | &wait "${last_pid}"
    fi
fi
if [ ! -f "$(which unzip)" ]; then
    echo 'unzip needs to be installed to run this script.'
    return | &wait "${last_pid}"
    if [[ $? != 0 ]]; then
        return | &wait "${last_pid}"
    fi
else
    if [ ! -f "$(which axel)" ]; then
        if [ ! -f "$(which curl)" ]; then
            echo 'At least axel or curl needs to be instaled to run this script.'
            return | &wait "${last_pid}"
            if [[ $? != 0 ]]; then
                return | &wait "${last_pid}"
            fi
        fi
    fi
fi

if [ "${PERSONAL_WORK_DIR}" != "" ] && [ -d "${PERSONAL_WORK_DIR}" ]; then
    mkdir -p ${PERSONAL_WORK_DIR}/vlang-latest && wait "${last_pid}"
    cd "${PERSONAL_WORK_DIR}/vlang-latest"
elif [ ! -d "${START_DIR}/tmp" ]; then
    mkdir tmp && wait "${last_pid}"
else
    cd tmp || return
fi
if [ -f vlang-latest/update.sh ]; then
    cd vlang-latest || return
    echo 'Updating PKGBUILD and ".SRCINFO"...'
    ./update.sh && wait "${last_pid}"
    if [[ $? != 0 ]]; then
        echo 'Sorry, an external error occured...'
        notify-send "update.sh failed... That's not supposed to happen... >_>" --app-name=update.sh --icon=folder-important --urgency=critical
        cd "${START_DIR}" || return
        return | &wait "${last_pid}"
        if [[ $? != 0 ]]; then
            return | &wait "${last_pid}"
        fi
    fi
    if [ -f $(which paru) ]; then
        paru --upgrade --install --removemake --cleanafter --rebuild --sudoloop --noconfirm && wait "${last_pid}"
        if [[ $? != 0 ]]; then
            echo 'Sorry, an error occured...'
            notify-send 'V-lang install/update failed...' --app-name=paru --icon=folder-important --urgency=critical
            cd "${START_DIR}" || return
            return | &wait "${last_pid}"
            if [[ $? != 0 ]]; then
                return | &wait "${last_pid}"
            fi
        fi
        notify-send 'V-lang install/update complete' --app-name=paru --icon=package
        rm ./*.tar.* && wait "${last_pid}"
        cd "${START_DIR}" || return
        return | &wait "${last_pid}"
        if [[ $? != 0 ]]; then
            return | &wait "${last_pid}"
        fi
    else
        makepkg -sfcCr --noconfirm && wait "${last_pid}"
        if [[ $? != 0 ]]; then
            echo 'Sorry, an error occured...'
            notify-send 'V-lang install/update failed...' --app-name=makepkg --icon=folder-important --urgency=critical
            cd "${START_DIR}" || return
            return | &wait "${last_pid}"
            if [[ $? != 0 ]]; then
                return | &wait "${last_pid}"
            fi
        fi
        notify-send 'V-lang install/update complete' --app-name=makepkg --icon=package
        rm ./*.tar.* && wait "${last_pid}"
        cd "${START_DIR}" || return
        return | &wait "${last_pid}"
        if [[ $? != 0 ]]; then
            return | &wait "${last_pid}"
        fi
    fi
else
        if [ ! -f "$(which git)" ]; then
            echo 'This script requires git to function; please install it.'
            cd ${START_DIR}
            return | &wait "${last_pid}"
            if [[ $? != 0 ]]; then
                return | &wait "${last_pid}"
            fi
        fi
        git clone --recursive --recurse-submodules https://gitlab.com/N3k0-san/vlang-latest.sh
        cd vlang-latest || return
        echo 'Updating PKGBUILD and ".SRCINFO"...'
        ./update.sh && wait "${last_pid}"
        if [[ $? != 0 ]]; then
            echo 'Sorry, an external error occured...'
            notify-send "update.sh failed... That's not supposed to happen... >_>" --app-name=update.sh --icon=folder-important --urgency=critical
            cd "${START_DIR}" || return
            return | &wait "${last_pid}"
            if [[ $? != 0 ]]; then
                return | &wait "${last_pid}"
            fi
        fi
        echo 'Building package...'
        if [ -f $(which paru) ]; then
            paru --upgrade --install --removemake --cleanafter --rebuild --sudoloop --noconfirm && wait "${last_pid}"
            if [[ $? != 0 ]]; then
                echo 'Sorry, an error occured...'
                notify-send 'V-lang install/update failed...' --app-name=paru --icon=folder-important --urgency=critical
                cd "${START_DIR}" || return
                return | &wait "${last_pid}"
                if [[ $? != 0 ]]; then
                    return | &wait "${last_pid}"
                fi
            fi
            notify-send 'V-lang install/update complete' --app-name=paru --icon=package
            rm ./*.tar.* && wait "${last_pid}"
            cd "${START_DIR}" || return
            return | &wait "${last_pid}"
            if [[ $? != 0 ]]; then
                return | &wait "${last_pid}"
            fi
        else
            makepkg -sfcCr --noconfirm && wait "${last_pid}"
            if [[ $? != 0 ]]; then
                echo 'Sorry, an error occured...'
                notify-send 'V-lang install/update failed...' --app-name=makepkg --icon=folder-important --urgency=critical
                cd "${START_DIR}" || return
                return | &wait "${last_pid}"
                if [[ $? != 0 ]]; then
                    return | &wait "${last_pid}"
                fi
            fi
            notify-send 'V-lang install/update complete' --app-name=makepkg --icon=package
            rm ./*.tar.* && wait "${last_pid}"
            cd "${START_DIR}" || return
            return | &wait "${last_pid}"
            if [[ $? != 0 ]]; then
                return | &wait "${last_pid}"
            fi
        fi
    fi
fi
